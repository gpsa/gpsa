import java.io.*;
import java.util.regex.Pattern;

public class Converter {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {

		if (args.length != 1) {
			System.out.println("Usage: java Converter filename....");
			System.exit(0);
		}

		String filename = args[0];
		String outfilename = filename + "x";

		BufferedReader br = new BufferedReader(new InputStreamReader(
				new FileInputStream(new File(filename))));
		DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(
				new FileOutputStream(new File(outfilename))));

		String line = null;
		Pattern p = Pattern.compile("\\s");
		String[] edge = null;
		int from, to;
		int numEdges = 0;
		int maxid = 0;
		while ((line = br.readLine()) != null) {
			numEdges++;
			edge = p.split(line);
			from = Integer.valueOf(edge[0]);
			to = Integer.valueOf(edge[1]);
			if (from > maxid)
				maxid = from;
			if (to > maxid)
				maxid = to;

			dos.writeInt(from);
			dos.writeInt(to);

		}
		dos.flush();

		br.close();
		dos.close();

		String inifile = outfilename + ".ini";

		BufferedWriter bos = new BufferedWriter(new FileWriter(
				new File(inifile)));
		bos.write("[graph]\n");
		bos.write("name=" + outfilename);
		bos.write("type=2");
		bos.write("vertices=" + (maxid + 1));
		bos.write("edges=" + numEdges);
		bos.flush();
		bos.close();

	}

}
