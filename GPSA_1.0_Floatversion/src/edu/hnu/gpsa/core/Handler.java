package edu.hnu.gpsa.core;

public interface Handler {
	
	float init(int sequence);
	float compute(float val,float mVal);

}
